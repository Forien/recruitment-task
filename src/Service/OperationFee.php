<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Service;

use Carbon\Carbon;
use WSzulc\CommissionTask\Exceptions\BaseException;
use WSzulc\CommissionTask\Exceptions\UnhandledClientTypeException;
use WSzulc\CommissionTask\Exceptions\UnhandledOperationException;
use WSzulc\CommissionTask\Exceptions\UnknownCurrencyException;
use WSzulc\CommissionTask\Interfaces\DataProvider;
use WSzulc\CommissionTask\Interfaces\RateProvider;
use WSzulc\CommissionTask\Providers\ConfigProvider;

class OperationFee
{
    private DataProvider $dataProvider;
    private ConfigProvider $config;
    private RateProvider $rateProvider;

    private array $cached_operations = [];

    public function __construct(DataProvider $dataProvider, ConfigProvider $config, RateProvider $rateProvider)
    {
        $this->dataProvider = $dataProvider;
        $this->config = $config;
        $this->rateProvider = $rateProvider;
    }

    /**
     * Loops over operations provided by Data Provider and calls process method for each line (operation)
     *
     * @return array
     */
    public function process(): array
    {
        $output = [];

        while (null !== $operation = $this->dataProvider->getOperation()) {
            try {
                $output[] = $this->processOperation($operation);
            } catch (BaseException $exception) {
                $output[] = $exception->getMessage();
            }
        }

        return $output;
    }

    /**
     * Processes single operation and returns calculated and formatted fee
     *
     * @param array $operation
     *
     * @return string
     * @throws UnhandledClientTypeException
     * @throws UnhandledOperationException
     * @throws UnknownCurrencyException
     */
    private function processOperation(array $operation): string
    {
        $fee = null;

        // Delegate calculations to specific methods based on operation type
        if ($operation) {
            $fee = match ($operation['operation_type']) {
                'deposit' => $this->calculateDeposit($operation),
                'withdraw' => $this->calculateWithdrawal($operation),
                default => throw new UnhandledOperationException($operation['operation_type']),
            };
        }

        // Round and format currency based on it's configuration
        $decimals = $this->getCurrencyDecimals($operation['operation_currency']);
        $fee = Math::round($fee, $decimals);

        return number_format($fee, $decimals, '.', '');
    }

    /**
     * Calculates fee for deposit operations
     *
     * @param array $operation
     * @return float
     */
    private function calculateDeposit(array $operation): float
    {
        $feePercent = $this->config->depositFee;

        return $operation['operation_amount'] * $feePercent;
    }

    /**
     * Delegates operation to specific methods based on client type and returns the calculated fee
     *
     * @param array $operation
     *
     * @return float
     * @throws UnhandledClientTypeException
     * @throws UnknownCurrencyException
     */
    private function calculateWithdrawal(array $operation): float
    {
        return match ($operation['user_type']) {
            'private' => $this->calculatePrivateWithdrawal($operation),
            'business' => $this->calculateBusinessWithdrawal($operation),
            default => throw new UnhandledClientTypeException($operation['user_type']),
        };
    }

    /**
     * Calculates fee for a withdrawal of a business client
     *
     * @param array $operation
     * @return float
     */
    private function calculateBusinessWithdrawal(array $operation): float
    {
        $feePercent = $this->config->businessCommissionFee;

        return $operation['operation_amount'] * $feePercent;
    }

    /**
     * Calculates fee for a withdrawal of a private client
     *
     * @param array $operation
     * @return float
     * @throws UnknownCurrencyException
     */
    private function calculatePrivateWithdrawal(array $operation): float
    {
        $feePercent = $this->config->privateCommissionFee;
        $freeWithdrawals = $this->config->privateWeeklyFreeWithdrawals;

        $userId = $operation['user_id'];
        $amount = $operation['operation_amount'];
        $currency = $operation['operation_currency'];
        $pastOperations = $this->getClientPastOperations($userId);

        $date = Carbon::parse($operation['date']);
        $week = $date->isoFormat('G-WW');
        $opsInWeek = $this->getClientOperationsInWeek($pastOperations, $week);

        // if client has free withdrawals that week, check the free amount and calculate only excess amount used for fee
        if ($opsInWeek < $freeWithdrawals) {
            $freeAmount = $this->getClientFreeAmountLeftInWeek($pastOperations, $week);
            $rate = $this->rateProvider->getRate($currency);
            $freeAmountInCurrency = $freeAmount * $rate;

            $excessAmount = $amount - $freeAmountInCurrency;
            $amount = max(0, $excessAmount);
        }

        $pastOperations[$week][] = $operation;
        $this->cached_operations[$userId] = $pastOperations;

        return $amount * $feePercent;
    }

    /**
     * Retrieves cached operations of a given user
     *
     * @param $user_id
     * @return array
     */
    private function getClientPastOperations($user_id): array
    {
        $pastOperations = [];
        if (array_key_exists($user_id, $this->cached_operations)) {
            $pastOperations = $this->cached_operations[$user_id];
        }

        return $pastOperations;
    }

    /**
     * Returns count of operations performed by a given user in a given week.
     *
     * @param array $pastOperations
     * @param string $week
     *
     * @return int
     */
    private function getClientOperationsInWeek(array $pastOperations, string $week): int
    {
        $operationCount = 0;

        if (array_key_exists($week, $pastOperations)) {
            $operationCount = count($pastOperations[$week]);
        }

        return $operationCount;
    }

    /**
     * Returns amount of commission-free amount left in any given week for an user.
     * Amount is provided in EUR.
     *
     * @param array $pastOperations
     * @param string $week
     *
     * @return float
     * @throws UnknownCurrencyException
     */
    private function getClientFreeAmountLeftInWeek(array $pastOperations, string $week): float
    {
        $freeAmountLeft = $this->config->privateWeeklyFreeAmount;

        if (array_key_exists($week, $pastOperations)) {
            foreach ($pastOperations[$week] as $operation) {
                $amount = $operation['operation_amount'];
                $rate = $this->rateProvider->getRate($operation['operation_currency']);
                $freeAmountLeft -= ($amount / $rate);

                if ($freeAmountLeft <= 0) {
                    break;
                }
            }

            $freeAmountLeft = max(0, $freeAmountLeft);
        }

        return $freeAmountLeft;
    }

    /**
     * Checks the config and retrieves how many (if at all) decimal places the currency format should be
     *
     * @param string $currency
     *
     * @return int
     */
    private function getCurrencyDecimals(string $currency): int
    {
        $currencyDecimals = $this->config->currencyDecimals;

        if (array_key_exists($currency, $currencyDecimals)) {
            return $currencyDecimals[$currency];
        }

        return $currencyDecimals['default'];
    }
}
