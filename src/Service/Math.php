<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Service;

abstract class Math
{
    /**
     * Rounds the provided number up (ceil) with a given precision (decimal places)
     *
     * @param float $number
     * @param int $precision
     *
     * @return float
     */
    public static function round(float $number, int $precision = 0): float
    {
        $power = pow(10, $precision);
        $number = $number * $power;
        $number = ceil($number);

        return $number / $power;
    }
}
