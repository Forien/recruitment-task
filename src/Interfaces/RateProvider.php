<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Interfaces;

use WSzulc\CommissionTask\Exceptions\UnknownCurrencyException;

interface RateProvider
{
    /**
     * @throws UnknownCurrencyException
     */
    public function getRate(string $currency): float;
}
