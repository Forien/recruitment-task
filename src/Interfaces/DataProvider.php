<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Interfaces;

interface DataProvider
{
    /**
     * Retrieve a single operation from Data Source.
     */
    public function getOperation(): ?array;
}
