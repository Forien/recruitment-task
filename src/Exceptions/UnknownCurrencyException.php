<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Exceptions;

class UnknownCurrencyException extends BaseException
{
    public function __construct(string $currency = '')
    {
        $message = "Unknown currency '{$currency}' provided, skipping.";

        parent::__construct($message);
    }
}
