<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Exceptions;

class UnhandledOperationException extends BaseException
{
    public function __construct(string $operation = '')
    {
        $message = "Unknown operation '{$operation}' provided, skipping.";

        parent::__construct($message);
    }
}
