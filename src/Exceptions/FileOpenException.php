<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Exceptions;

class FileOpenException extends BaseException
{
    protected $message = 'There was an error opening file, please check file permissions';
}
