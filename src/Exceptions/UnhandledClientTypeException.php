<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Exceptions;

class UnhandledClientTypeException extends BaseException
{
    public function __construct(string $type = '')
    {
        $message = "Unknown client type '{$type}' provided, skipping.";

        parent::__construct($message);
    }
}
