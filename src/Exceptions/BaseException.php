<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Exceptions;

use Exception;

class BaseException extends Exception
{
}
