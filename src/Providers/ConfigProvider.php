<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Providers;

class ConfigProvider
{
    public float $depositFee;
    public float $businessCommissionFee;
    public float $privateCommissionFee;
    public float $privateWeeklyFreeAmount;
    public float $privateWeeklyFreeWithdrawals;
    public array $currencyDecimals;

    public function __construct(array $config)
    {
        $this->depositFee = $config['deposit_fee'];
        $this->businessCommissionFee = $config['business_commission_fee'];
        $this->privateCommissionFee = $config['private_commission_fee'];
        $this->privateWeeklyFreeAmount = $config['private_weekly_free_amount'];
        $this->privateWeeklyFreeWithdrawals = $config['private_weekly_free_withdrawals'];

        $this->currencyDecimals = $config['currency_decimals'];
    }
}
