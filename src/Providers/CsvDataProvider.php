<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Providers;

use WSzulc\CommissionTask\Exceptions\FileOpenException;
use WSzulc\CommissionTask\Interfaces\DataProvider;

class CsvDataProvider implements DataProvider
{
    private mixed $file;

    /**
     * @param string $inputFilePath file path to the CSV input file with operation details
     *
     * @throws FileOpenException
     */
    public function __construct(string $inputFilePath)
    {
        $this->openFile($inputFilePath);
    }

    /**
     * @throws FileOpenException
     */
    private function openFile(string $inputFilePath): void
    {
        $handle = fopen($inputFilePath, 'r');

        if (false === $handle) {
            throw new FileOpenException();
        }

        $this->file = $handle;
    }

    public function getOperation(): ?array
    {
        $line = fgetcsv($this->file);

        if (!$line) {
            return null;
        }

        return [
            'date' => $line[0],
            'user_id' => $line[1],
            'user_type' => $line[2],
            'operation_type' => $line[3],
            'operation_amount' => $line[4],
            'operation_currency' => $line[5],
        ];
    }
}
