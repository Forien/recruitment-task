<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Providers;

use WSzulc\CommissionTask\Exceptions\UnknownCurrencyException;
use WSzulc\CommissionTask\Interfaces\RateProvider;

/**
 * Live rates retrieved from external API.
 */
class LiveRateProvider implements RateProvider
{
    private string $api = 'https://developers.paysera.com/tasks/api/currency-exchange-rates';
    private $rates;

    public function __construct()
    {
        $response = file_get_contents($this->api);
        $object = json_decode($response);
        $this->rates = $object->rates;
    }

    /**
     * @throws UnknownCurrencyException
     */
    public function getRate(string $currency): float
    {
        if (property_exists($this->rates, $currency)) {
            return $this->rates->$currency;
        }

        throw new UnknownCurrencyException($currency);
    }
}
