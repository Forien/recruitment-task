<?php

declare(strict_types=1);

use WSzulc\CommissionTask\Exceptions\BaseException;
use WSzulc\CommissionTask\Providers\ConfigProvider;
use WSzulc\CommissionTask\Providers\CsvDataProvider;
use WSzulc\CommissionTask\Providers\LiveRateProvider;
use WSzulc\CommissionTask\Service\OperationFee;

include __DIR__ . '/vendor/autoload.php';

if (1 === $argc) {
    echo 'Please provide a path to a CSV file with input values.' . PHP_EOL;

    return;
}

$filepath = $argv[1];

if (false === file_exists($filepath)) {
    echo "File {$filepath} does not exist." . PHP_EOL;

    return;
}

try {
    $dataProvider = new CsvDataProvider($filepath);
    $configArray = require 'config.php';
    $config = new ConfigProvider($configArray);
    $rateProvider = new LiveRateProvider();

    $commission = new OperationFee($dataProvider, $config, $rateProvider);

    $fees = $commission->process();

    // Output fees, one per line
    foreach ($fees as $fee) {
        echo $fee . PHP_EOL;
    }
} catch (BaseException $exception) {
    echo $exception->getMessage() . PHP_EOL;
}
