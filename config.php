<?php

declare(strict_types=1);

return [
    'deposit_fee' => 0.0003,
    'business_commission_fee' => 0.005,
    'private_commission_fee' => 0.003,
    'private_weekly_free_amount' => 1000,
    'private_weekly_free_withdrawals' => 3,

    'currency_decimals' => [
        'default' => 2,
        'JPY' => 0,
    ],
];
