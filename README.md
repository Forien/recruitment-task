# Commission task solution

## Platform requirements
Project was developed on platform with following specs:
* CLI PHP version: `8.1`
* Composer version: `2.5.7`
* Configuration file `php.ini` should have `allow_url_fopen` setting set to `On`
  * For Ubuntu, file can be found under `\etc\php\8.1\cli\php.ini` path

## Usage
### Initialization
Before running the app, perform a composer install:
```bash
$ composer install
```

### Running script
To run script, use the following command:
```bash
$ php commission.php input.csv
```
You can change `input.csv` to any other path (relative or absolute)

### Running tests
To run tests, use the one of the following commands:
```bash
$ composer run phpunit      # runs phpunit tests
$ composer run test-cs      # runs a CS test
$ composer run test         # runs both tests 
```


#### Notes on PHP version
* I tried making project on 7.4 at first, but I could not make the provided skeleton to work on fresh install of PHP/Composer (Windows 11 WSL with Ubuntu 22.04 LTS)
* Unless I forgot about something, all code should be PHP 7.4 compatible except for the `match` expression
  * I used it because of stronger identity check instead of equality check found in `switch..case`
  * It's easy to change back to `switch..case` if you can't run PHP 8.1