<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Tests\Service;

use PHPUnit\Framework\TestCase;
use WSzulc\CommissionTask\Service\Math;

class MathTest extends TestCase
{
    /**
     * @dataProvider dataProviderForRoundUpTesting
     */
    public function testRoundUp(float $number, int $decimals, float $expectation)
    {
        $this->assertEquals(
            $expectation,
            Math::round($number, $decimals)
        );
    }

    public static function dataProviderForRoundUpTesting(): array
    {
        return [
            'round up whole number' => [12.2114, 0, 13],
            'round up to 2 decimal places, upper half' => [0.565656, 2, 0.57],
            'round up to 2 decimal places, lower half' => [2.1111, 2, 2.12],
            'round up to 6 decimal places' => [3.14159265359, 6, 3.141593],
        ];
    }
}
