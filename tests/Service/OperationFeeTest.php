<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Tests\Service;

use PHPUnit\Framework\TestCase;
use WSzulc\CommissionTask\Providers\ConfigProvider;
use WSzulc\CommissionTask\Service\OperationFee;
use WSzulc\CommissionTask\Tests\Mockups\CsvMockup;
use WSzulc\CommissionTask\Tests\Mockups\ExampleRateProvider;

class OperationFeeTest extends TestCase
{
    private OperationFee $operationFee;
    private array $expectation;

    protected function setUp(): void
    {
        $testInputData = <<<EOT
2014-12-31,4,private,withdraw,1200.00,EUR
2015-01-01,4,private,withdraw,1000.00,EUR
2016-01-05,4,private,withdraw,1000.00,EUR
2016-01-05,1,private,deposit,200.00,EUR
2016-01-06,2,business,withdraw,300.00,EUR
2016-01-06,1,private,withdraw,30000,JPY
2016-01-07,1,private,withdraw,1000.00,EUR
2016-01-07,1,private,withdraw,100.00,USD
2016-01-10,1,private,withdraw,100.00,EUR
2016-01-10,2,business,deposit,10000.00,EUR
2016-01-10,3,private,withdraw,1000.00,EUR
2016-02-15,1,private,withdraw,300.00,EUR
2016-02-19,5,private,withdraw,3000000,JPY
EOT;

        $expectedResults = <<<RES
0.60
3.00
0.00
0.06
1.50
0
0.70
0.30
0.30
3.00
0.00
0.00
8612
RES;

        // Load expected results as an array
        $expectedResults = str_replace(["\r\n", "\r"], "\n", $expectedResults);
        $this->expectation = explode("\n", $expectedResults);

        // load mockup class for providing operation data
        $dataProvider = new CsvMockup($testInputData);
        // create test config class
        $config = new ConfigProvider([
            'deposit_fee' => 0.0003,
            'business_commission_fee' => 0.005,
            'private_commission_fee' => 0.003,
            'private_weekly_free_amount' => 1000,
            'private_weekly_free_withdrawals' => 3,

            'currency_decimals' => [
                'default' => 2,
                'JPY' => 0,
            ],
        ]);
        // Load mockup class for rates
        $rateProvider = new ExampleRateProvider();
        $this->operationFee = new OperationFee($dataProvider, $config, $rateProvider);
    }

    public function testProcess()
    {
        $result = $this->operationFee->process();

        $this->assertEquals($this->expectation, $result);
    }
}
