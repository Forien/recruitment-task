<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Tests\Mockups;

use WSzulc\CommissionTask\Interfaces\DataProvider;

/**
 * Mockup DataProvider used for testing
 */
class CsvMockup implements DataProvider
{
    private mixed $data;

    public function __construct($data)
    {
        // just to make sure there is compatibility between systems
        $string = str_replace(["\r\n", "\r"], "\n", $data);

        // split provided data into array (split lines)
        $this->data = explode("\n", $string);
    }

    public function getOperation(): ?array
    {
        $line = array_shift($this->data);

        if (null === $line) {
            return null;
        }

        // split each line into array to mimic CSV
        $line = explode(',', $line);

        return [
            'date' => $line[0],
            'user_id' => $line[1],
            'user_type' => $line[2],
            'operation_type' => $line[3],
            'operation_amount' => $line[4],
            'operation_currency' => $line[5],
        ];
    }
}
