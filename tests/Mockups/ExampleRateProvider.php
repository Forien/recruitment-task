<?php

declare(strict_types=1);

namespace WSzulc\CommissionTask\Tests\Mockups;

use WSzulc\CommissionTask\Exceptions\UnknownCurrencyException;
use WSzulc\CommissionTask\Interfaces\RateProvider;

/**
 * Mockup RateProvider with static rates used for checking the example data and for tests.
 */
class ExampleRateProvider implements RateProvider
{
    /**
     * @throws UnknownCurrencyException
     */
    public function getRate(string $currency): float
    {
        switch ($currency) {
            case 'EUR':
                return 1;
            case 'JPY':
                return 129.53;
            case 'USD':
                return 1.1497;
            default:
                throw new UnknownCurrencyException($currency);
        }
    }
}
