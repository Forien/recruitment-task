<?php

declare(strict_types=1);

include __DIR__ . '/vendor/autoload.php';


$finder = PhpCsFixer\Finder::create()
    ->exclude('somedir')
    ->notPath('src/Symfony/Component/Translation/Tests/fixtures/resources.php')
    ->in(__DIR__);

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@PSR12' => true,
    'no_alternative_syntax' => true,
    'strict_comparison' => true,
    'strict_param' => true,
    'declare_strict_types' => true,
    'yoda_style' => false,
    'array_syntax' => ['syntax' => 'short'],
])
    ->setFinder($finder)
    ->setUsingCache(false)
    ->setRiskyAllowed(true);
